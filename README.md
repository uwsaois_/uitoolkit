# UW System UI Toolkit

A web front-end toolkit based on [Bootstrap](http://getbootstrap.com) for designing and developing modern, mobile-first web interfaces for initiatives, applications, and licensed affiliates of the [University of Wisconsin System and the University of Wisconsin System Administration](https://www.wisconsin.edu).

## Who is it for?

This toolkit will allow internal UW System units, licensed affiliates, and approved external organizations to implement our brand, providing a consistent experience for users across the websites and applications provided for and by UW System. The toolkit facilitates this by providing a framework of HTML, CSS, and JS which has been designed to meet all branding and style requirements when implemented according to specifications.

## Who manages it?

The Digital Communications Team at University of Wisconsin System Administration under the auspices of the Office of University Relations has been charged by systemwide leadership to develop, steward, and protect a consistent and coherent brand that represents the entire UW System and its member institutions. The digital aspects of this brand are reflected in those works published by the Digital Communications Team and the associated documentation.