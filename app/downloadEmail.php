<?php
/**
 * Created by PhpStorm.
 * User: Rachel Klingler
 * Date: 4/28/2015
 * Time: 2:21 PM
 */

// Check for empty fields
if(empty($_POST['name'])          ||
    empty($_POST['altname'])          ||
    empty($_POST['email'])         ||
    empty($_POST['altemail'])         ||
    empty($_POST['org'])    ||
    empty($_POST['type'])    ||
    !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))

{
    echo "No arguments Provided!";
    return false;
}


$name = sanitizeString($_POST['name']);
$alt_name = sanitizeString($_POST['altname']);
$email_address = sanitizeString($_POST['email']);
$alt_email_address = sanitizeString($_POST['altemail']);
$org = sanitizeString($_POST['org']);
$type = sanitizeString($_POST['type']);

// Create the email and send the message to webteam
$to = 'webteam@uwsa.edu';
$email_subject = "New UI Toolkit Download:  $name at $org";
$email_body = "Someone has downloaded the UI Toolkit.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\nOrganization: $org\n\nAlternate Name: $alt_name\n\nAlternate Email: $alt_email_address\n\nType of Download: $type";
$headers = "From: webadmin@uwsa.edu\n"; // This is the email address the generated message will be from.
$headers .= "Reply-To: $email_address";
mail($to,$email_subject,$email_body,$headers);

// generate URL based on the type chosen
if ($type=="Production") {
    $url="http://ui.web.uwsa.edu/pack/production.zip";
}
elseif ($type=="Developer") {
    $url="http://ui.web.uwsa.edu/pack/developer.zip. \n\nIf you prefer, you can build can build directly from our Git repository at http://code.web.uwsa.edu/projects/ADM/repos/uitoolkit/browse";
}
elseif ($type=="WordPress") {
    $url="https://bitbucket.org/uwsaois_/basicwp_uwsatheme/downloads";
}

// Create the email and send the message to the requestor
$to = $email_address;
$email_subject = "UWSA UI Toolkit Download";
$email_body = "Hello $name,\n\nThank you for using the UWSA UI Toolkit. The toolkit can be downloaded at $url.\n\nFor your records, a copy of the Memorandum of Understanding can be found online at http://ui.web.uwsa.edu/mou.pdf.\n\nPlease contact us at webteam@uwsa.edu with further questions.\n\nUWSA Digital Communication Team";
$headers = "From: webteam@uwsa.edu\n"; // This is the email address the generated message will be from.
$headers .= "Reply-To: webteam@uwsa.edu";
mail($to,$email_subject,$email_body,$headers);

//readfile("about.html");
//$message = "Thank you! An email has been sent.";
//echo "<script type='text/javascript'>alert('$message');</script>";

return true;


function sanitizeString($var)
{
    $var = stripslashes($var);
    $var = strip_tags($var);
    $var = htmlentities($var);
    return $var;
}
?>
